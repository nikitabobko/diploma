package diploma

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.FileReader
import java.util.regex.Pattern
import kotlin.math.abs

data class Batch(
  val sendHits: List<Int>,
  val reason: BatchEndedReason,
  val trace: StackTrace,
  val rank: Int,
  val batchId: Int,
  val timeMillis: Long
) {
  val normalizedSendHits by lazy {
    val allHitsCount = sendHits.sum()
    sendHits.map { 1.0 * it / allHitsCount }
  }

  private data class RawBatch(val hits: List<Int>, val reason: BatchEndedReason, val trace: List<String>, val timeMillis: Long)

  companion object {
    private val gson = Gson()
    private val typeToken = object : TypeToken<RawBatch>() {}.type
    private val fileNamePattern = Pattern.compile("""^dump_rank-(\d+)_batch-(\d+)\.json$""")

    fun fromFile(jsonDumpFile: File): Batch {
      val matcher = fileNamePattern.matcher(jsonDumpFile.name).also { check(it.matches()) }

      return FileReader(jsonDumpFile).use { reader ->
        gson.fromJson<RawBatch>(reader, typeToken)!!.run {
          val rank = matcher.group(1).toInt()
          val batchId = matcher.group(2).toInt()
          return@run Batch(hits, reason, StackTrace(trace.map { StackTraceItem.from(it) }), rank, batchId, timeMillis)
        }
      }
    }
  }
}

infix fun Batch.sameAs(other: Batch): Boolean {
  checkCompatible(this, other)
  return this.normalizedSendHits.asSequence()
    .zip(other.normalizedSendHits.asSequence())
    .all { abs(it.first - it.second) <= 0.3 }
}

operator fun Batch.plus(other: Batch): Batch {
  checkCompatible(this, other)
  val right = if (this.batchId > other.batchId) this else other
  val mergedHits = this.sendHits.asSequence().zip(other.sendHits.asSequence()).map { it.first + it.second }.toList()
  return Batch(mergedHits, right.reason, right.trace, rank, right.batchId, right.timeMillis)
}

fun checkCompatible(foo: Batch, bar: Batch) {
  check(foo.sendHits.size == bar.sendHits.size) {
    "$foo and $bar are not compatible"
  }
  check(foo.rank == bar.rank) {
    "$foo and $bar are not compatible"
  }
}

fun List<Batch>.reduceBatches(mergePredicate: (Batch, Batch) -> Boolean): List<Batch> {
  val ans = ArrayList<Batch>()
  var current: Batch? = null
  var batchId = 0
  for (anotherBatch in this) {
    if (current == null) {
      current = anotherBatch
    } else {
      if (mergePredicate(anotherBatch, current)) {
        current += anotherBatch
      } else {
        ans.add(current.copy(batchId = batchId++))
        current = anotherBatch
      }
    }
  }
  if (current != null) {
    ans.add(current.copy(batchId = batchId))
  }
  return ans
}
