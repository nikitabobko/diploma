package diploma

import java.io.File
import java.util.regex.Pattern

private fun String.exec(): String {
  val process = ProcessBuilder("/bin/bash", "-c", this).start()
  val result = String(process.inputStream.readBytes())
  process.errorStream.readBytes()
  process.waitFor()
  return result
}

private data class FuncInBin(
  val file: File,
  /**
   * Please be careful. It's not function name. It's function symbol name. E.g. function can be named like
   * "secondStagePrimeNumbersSearch" but symbol at the same time is "_Z29secondStagePrimeNumbersSearchiPPc"
   */
  val funcSymbolName: String
)

data class StackTraceItem(
  val fileName: String,
  val funcName: String,
  val relativeOffsetInsideFunc: Long
) {
  companion object {
    /**
     * `backtrace_symbols` item pattern
     * pattern example:
     * ```
     * file.so(funcSymbolName+0x161f) [0x7f0f083d961f]
     * ```
     */
    @Suppress("RegExpRedundantEscape")
    private val backtraceItemPattern = Pattern.compile("""^(.+)\s*\((.*)\+(.+)\)\s*\[(.+)\]$""")

    /**
     * pattern examples:
     * ```
     * Line 41 of "sighandler.c" starts at address 0x13dc <func_a> and ends at 0x13e8 <func_a+12>
     * Line 321 of "main.cpp" starts at address 0x14199 <main(int, char**)> and ends at 0x141a8 <main(int, char**)+15>.
     * ```
     */
    private val gdbPattern = Pattern.compile("""^[\s\S]*?0x(.+?)\s*<.*>[\s\S]*$""")

    private val funcInBinToOffset = HashMap<FuncInBin, Long>()

    fun from(str: String): StackTraceItem {
      val matcher = backtraceItemPattern.matcher(str).also { check(it.matches()) }
      val funcName = matcher.group(2)
      val relativeOffsetInsideFunc = matcher.group(3)
        .substring(2) // trim leading 0x
        .toLong(radix = 16)

      return StackTraceItem(matcher.group(1), funcName, relativeOffsetInsideFunc)
    }

    private fun getFuncOffset(funcInBin: FuncInBin): Long? = funcInBinToOffset.getOrPut(funcInBin) {
      val absolutePath = funcInBin.file.absolutePath
      if (funcInBin.funcSymbolName.isEmpty() || absolutePath.isEmpty()) {
        return null
      }
      val gdbOutput = """gdb $absolutePath -ex "info line ${funcInBin.funcSymbolName}" -ex "quit"""".exec()

      val matcher = gdbPattern.matcher(gdbOutput).takeIf { it.matches() } ?: return null

      return@getOrPut matcher.group(1).toLong(radix = 16)
    }
  }

  fun toPresentableString(binFile: File?): String = if (binFile == null) {
    toString()
  } else {
    val fileNameAndLine = getFileNameAndLine(binFile)
    if (fileNameAndLine != null) {
      "${toString()} $fileNameAndLine"
    } else {
      toString()
    }
  }

  private fun getFileNameAndLine(binFile: File): String? {
    check(binFile.exists()) { "$binFile doesn't exist" }
    val funcOffset = getFuncOffset(FuncInBin(binFile, funcName)) ?: return null
    val absoluteOffset = funcOffset + relativeOffsetInsideFunc
    return """addr2line -e ${binFile.absolutePath} 0x${absoluteOffset.toString(radix = 16)}""".exec().trim()
  }
}
