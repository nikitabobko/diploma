package diploma

@Suppress("DataClassPrivateConstructor")
data class RemappingPoint private constructor(
  val stackTraces: List<StackTrace>,
  private val rankToTimeMillis: Map<Int, Long>
) {
  companion object {
    fun from(batches: List<Batch>): RemappingPoint {
      require(batches.isNotEmpty()) { "traces must contain at least one element" }
      val traces = batches.map { it.trace }
      return RemappingPoint(traces.distinct(), batches.groupBy { it.rank }.mapValues { it.value.single().timeMillis })
    }
  }

  fun rankToTimeMillis(rank: Int) = rankToTimeMillis.getOrElse(rank) {
    rankToTimeMillis.values.min()!!
  }
}

val RemappingPoint.isAmbiguous
  get() = stackTraces.size > 1
