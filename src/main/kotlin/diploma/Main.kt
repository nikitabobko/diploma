package diploma

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import java.io.File
import java.io.PrintWriter
import kotlin.math.abs

class MyArgs(parser: ArgParser) {
  val dumpDirFile by parser.storing(
    "--dump-dir-path",
    help = "Path to directory produced by mpi-tracer-run"
  ) { File(this) }

  val binFile by parser.storing(
    "--bin-path",
    help = "Path to binary which was traced. Required for demystifying stacks"
  ) { File(this) }.default(null as File?)

  val dirForCommunicationMatrices by parser.storing(
    "--output-dir",
    help = "Output directory for communication matrices. Communication matrices are not generated if this parameter is not specified"
  ) { File(this) }.default(null as File?)
}

const val MILLIS_EPS = 10L

@Suppress("unused")
enum class BatchEndedReason {
  REGULAR, EXCHANGE_NATURE, MPI_FINALIZE
}

fun main(args: Array<String>): Unit = mainBody {
  val parsedArgs: MyArgs = ArgParser(args).parseInto(::MyArgs).also {
    check(it.dumpDirFile.exists() && it.dumpDirFile.isDirectory && it.dumpDirFile.list()!!.isNotEmpty()) {
      "${it.dumpDirFile.absolutePath} supposed to exist and not to be empty directory"
    }
  }

  val files = parsedArgs.dumpDirFile.listFiles()!!
  val rankToBatches = files
    .map { Batch.fromFile(it) }
    .groupBy { it.rank }
    .mapValues { it.value.sortedBy { batch -> batch.batchId } }
  val batchesByRank = rankToBatches.map { it.value }

  checkBatchesByRankInvariants(batchesByRank)

  val reducedBatchesByRank = batchesByRank
    .map { it.reduceBatches() }
    .map { it.run { subList(0, size - 1) } }

  val remappingPoints = findRemappingPointsInReducedBatches(reducedBatchesByRank)

  outputRemappingPoints(remappingPoints, parsedArgs)
  outputCommunicationMatrices(parsedArgs, remappingPoints, rankToBatches)
}

private fun outputCommunicationMatrices(
  parsedArgs: MyArgs,
  remappingPoints: List<RemappingPoint>,
  rankToBatches: Map<Int, List<Batch>>
) {
  val dirForCommunicationMatrices = parsedArgs.dirForCommunicationMatrices ?: return
  dirForCommunicationMatrices.mkdir()

  val numOfProc = rankToBatches.size
  val rankToPrevIndices = (0 until numOfProc).groupBy { it }.mapValues { 0 }.toMutableMap()

  val remappingPointsWithLast = remappingPoints + listOf(RemappingPoint.from(rankToBatches.map { it.value.last() }))

  for (i in remappingPointsWithLast.indices) {
    val communicationMatrixFile = dirForCommunicationMatrices.resolve("remapping_point_${i}")
    val remappingPoint = remappingPointsWithLast[i]
    val until = rankToBatches.mapValues { entry ->
      val rank = entry.key
      val batchesOfOneRank = entry.value
      val timeMillis = remappingPoint.rankToTimeMillis(rank)
      return@mapValues batchesOfOneRank.indexOfFirst { it.timeMillis > timeMillis }.takeIf { it > 0 } ?: entry.value.size
    }
    check(until.size == numOfProc)
    val matrix = rankToBatches.mapValues { entry ->
      entry.value.subList(rankToPrevIndices[entry.key]!!, until[entry.key]!!).takeIf { it.isNotEmpty() }?.reduce { foo, bar -> foo + bar }
    }.toList().sortedBy { it.first }.map { it.second?.sendHits ?: List(numOfProc) { 0 } }
    PrintWriter(communicationMatrixFile).use { writer ->
      for (line in matrix) {
        for (item in line) {
          writer.print("$item\t")
        }
        writer.println()
      }
    }
    until.forEach { (rank, index) -> rankToPrevIndices[rank] = index }
  }
}

private fun checkBatchesByRankInvariants(batchesByRank: List<List<Batch>>) {
  for (batchesOfOneRank in batchesByRank) {
    check(batchesOfOneRank.sortedBy { it.timeMillis } == batchesOfOneRank) {
      "Sorting by batchId or time must be equivalent"
    }
    check(batchesOfOneRank.last().reason == BatchEndedReason.MPI_FINALIZE) {
      "${BatchEndedReason.MPI_FINALIZE} must be the latest reason to stop tracing. Probably your dumps are corrupted/traced program doesn't do MPI_Finalize"
    }
    check(batchesOfOneRank.all { it.rank == batchesOfOneRank.first().rank })
  }
}

private fun outputRemappingPoints(remappingPoints: List<RemappingPoint>, parsedArgs: MyArgs) {
  if (remappingPoints.isEmpty()) {
    println("No remapping points found")
  } else {
    for ((i, remappingPoint) in remappingPoints.withIndex()) {
      if (remappingPoint.isAmbiguous) {
        println("${(i + 1)}/${remappingPoints.size} ambiguous remapping point found:")
      } else {
        println("${(i + 1)}/${remappingPoints.size} remapping point found:")
      }
      for ((j, trace) in remappingPoint.stackTraces.withIndex()) {
        if (remappingPoint.isAmbiguous) {
          println("Version ${j + 1}/${remappingPoint.stackTraces.size}")
        }
        println(trace.toPresentableString(parsedArgs.binFile))
      }
    }
  }
}

fun List<Batch>.reduceBatches(): List<Batch> = this.reduceBatches { foo, bar -> foo sameAs bar }

private fun findRemappingPointsInReducedBatches(reducedBatchesByRank: List<List<Batch>>): List<RemappingPoint> {
  val numOfProc = reducedBatchesByRank.size
  val times = reducedBatchesByRank.flatten().map { it.timeMillis }.distinct().sorted()
  return times
    .map { time ->
      return@map reducedBatchesByRank.mapNotNull { batchesOfOneRank ->
        val index = batchesOfOneRank.binarySearch { (it.timeMillis - time).toInt() }.let { if (it >= 0) it else -it + 1 }
        val batch1 = if (index in batchesOfOneRank.indices) batchesOfOneRank[index] else null
        val batch2 = if (index - 1 in batchesOfOneRank.indices) batchesOfOneRank[index - 1] else null
        return@mapNotNull listOfNotNull(batch1, batch2).minBy { abs(it.timeMillis - time) }
      }.filter { abs(it.timeMillis - time) < MILLIS_EPS }
    }
    .filter { remappingPointsAtTime -> remappingPointsAtTime.size >= numOfProc / 2 }
    .map { remappingPointsAtTime -> RemappingPoint.from(remappingPointsAtTime) }
}
