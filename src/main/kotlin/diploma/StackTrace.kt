package diploma

import java.io.File

data class StackTrace(private val trace: List<StackTraceItem>) : List<StackTraceItem> by trace {
  fun toPresentableString(binFile: File?): String {
    return trace.joinToString("\n") { "  ${it.toPresentableString(binFile)}" }
  }
}

