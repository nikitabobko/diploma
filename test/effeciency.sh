#!/usr/bin/env sh
sizes=("1024x1024" "2048x2048" "4096x4096")
processes=(1 8 64 125)

for size in ${sizes[@]}; do
    m=$(echo $size | cut -d'x' -f1)
    n=$(echo $size | cut -d'x' -f2)

    start=$(cat results/${size}_1.stdout | head -n 1)

    x=$(cat results/${size}_125_mapping.stdout | head -n 1)
    x=$(echo "print(${start}/${x}/125)" | python)
    var="${m} & ${n} & $x"
    for p in ${processes[@]}; do
        x=$(cat results/${size}_${p}.stdout | head -n 1)
        x=$(echo "print(${start}/${x}/${p})" | python)
        var+=" & ${x}"
    done
    echo "$var \\\\"
    echo "\hline"
done
