#!/usr/bin/env sh

perform() {
    echo "Performing: $@"
    if ! $@; then
        echo "Cannot perform: $@"
        exit 1
    fi
}


sizes=("1024x1024" "2048x2048" "4096x4096")
processes=(1 8 64 125)

for p in ${processes[@]}; do
    for size in ${sizes[@]}; do 
        perform mpisubmit.bg -n ${p} -w 0:30:0 --stdout results/${size}_${p}.stdout --stderr results/${size}_${p}.stderr main.out ${size}.mat ${size}.mat results/${size}.reuslt        
    done
done
