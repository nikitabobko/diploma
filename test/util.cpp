#ifndef UTIL
#define UTIL

#include <iostream>
#include <vector>
#include <cstdlib>
#define loop(i, n) for(size_t i = 0; i < (size_t)(n); ++i)
// #define AT(i, j) (i)*column_count + (j)
typedef unsigned char uchar;
typedef unsigned long long ull;

template<typename T>
class Matrix {
public:
    T *matrix_pointer;
    int rows_count;
    int column_count;

    Matrix() : matrix_pointer(NULL), rows_count(0), column_count(0) {

    }

    void fill_with_zeros() {
        loop(i, rows_count) {
            loop(j, column_count) {
                at(i, j) = T();
            }
        }
    }

    Matrix(int n, int m) : matrix_pointer(new T[n * m]), rows_count(n), column_count(m) {
        fill_with_zeros();
    }

    __attribute__((always_inline)) T &at(int i, int j) {
        return matrix_pointer[i * column_count + j];
    }

    __attribute__((always_inline)) T at(int i, int j) const {
        return matrix_pointer[i * column_count + j];
    }

    Matrix<T> operator *(Matrix<T> &other) const {
        if (this->column_count != other.rows_count) {
            fprintf(stderr, "Cannot multiply %dx%d matrix on %dx%d matrix", this->rows_count, this->column_count,
                    other.rows_count, other.column_count);
            exit(1);
        }
        Matrix<T> ans(rows_count, other.column_count);
        for (int k = 0; k < column_count; ++k) {
            for (int i = 0; i < rows_count; ++i) {
                for (int j = 0; j < other.column_count; ++j) {
                   ans.at(i, j) += at(i,k)*other.at(k, j);
                }
            }
        }
        return ans;
    }

    void free() {
        delete[] matrix_pointer;
    }
};

template<typename T>
T read_from_binary(std::istream &input) {
    T t;
    input.read((char *)&t, sizeof(t));
    return t;
}

template<typename T>
void write_to_binary(std::ostream &output, const T &t) {
    output.write((char *)&t, sizeof(t));
}

template<typename T>
Matrix<T> read_matrix_from_binary_stream(std::istream &input) {
    // uchar type = read_from_binary<uchar>(input);
    int n = read_from_binary<int>(input);
    int m = read_from_binary<int>(input);
    Matrix<T> ret(n, m);

    loop(i, n) {
        loop(j, m) {
            ret.at(i, j) = read_from_binary<T>(input);
        }
    }
    return ret;
}

Matrix<float> read_float_matrix_from_binary_stream(std::istream &input) {
    return read_matrix_from_binary_stream<float>(input);
}

Matrix<double> read_double_matrix_from_binary_stream(std::istream &input) {
    return read_matrix_from_binary_stream<double>(input);
}

#endif
