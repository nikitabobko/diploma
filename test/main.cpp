#include <iostream>
#include <vector>
#include <set>
#include <cmath>
#include <fstream>
#include "mpi.h"
#include "util.cpp"

static const int NDIMS = 3;

enum FILE_OP {
    READ, WRITE
};

static double file_io_spent = 0.;

void sub_matrix_file_op(MPI_Comm comm, int i, int j, int dim, const char *filename, FILE_OP file_op,
                        Matrix<double> &matrix, int *whole_matrix_rows_count, int *whole_matrix_column_count) {
    double start = MPI_Wtime();
    MPI_File matrix_file;
    MPI_File_open(comm, (char *) filename, file_op == READ ? MPI_MODE_RDONLY : MPI_MODE_CREATE | MPI_MODE_WRONLY,
            MPI_INFO_NULL, &matrix_file);

    int n, m;
    if (file_op == READ) {
        char unused;
        MPI_File_read(matrix_file, &unused, 1, MPI_CHAR, MPI_STATUS_IGNORE);
        MPI_File_read(matrix_file, &n, 1, MPI_INT, MPI_STATUS_IGNORE);
        MPI_File_read(matrix_file, &m, 1, MPI_INT, MPI_STATUS_IGNORE);

        if (whole_matrix_rows_count != NULL) *whole_matrix_rows_count = n;
        if (whole_matrix_column_count != NULL) *whole_matrix_column_count = m;
    } else {
        n = *whole_matrix_rows_count;
        m = *whole_matrix_column_count;
        if (i == 0 && j == 0) {
            char type = 'd';
            MPI_File_write(matrix_file, &type, 1, MPI_CHAR, MPI_STATUS_IGNORE);
            MPI_File_write(matrix_file, &n, 1, MPI_INT, MPI_STATUS_IGNORE);
            MPI_File_write(matrix_file, &m, 1, MPI_INT, MPI_STATUS_IGNORE);
        }
    }

    int array_of_sizes[] = {m};
    int array_of_subsizes[] = {m/dim + (j == dim - 1)*(m%dim)};
    int array_of_starts[] = {m/dim*j};

    if (file_op == READ) {
        matrix = Matrix<double>(n/dim + (i == dim - 1)*(n%dim), m/dim + (j == dim - 1)*(m%dim));
    }

    MPI_Datatype mytype;
    MPI_Type_create_subarray(1, array_of_sizes, array_of_subsizes, array_of_starts, MPI_ORDER_C, MPI_DOUBLE, &mytype);
    MPI_Type_commit(&mytype);

    MPI_File_set_view(matrix_file, sizeof(char) + 2*sizeof(int) + n/dim*i*m*sizeof(double), MPI_DOUBLE, mytype,
            "native", MPI_INFO_NULL);

    if (file_op == READ) {
        MPI_File_read(matrix_file, matrix.matrix_pointer, matrix.rows_count * matrix.column_count, MPI_DOUBLE, MPI_STATUS_IGNORE);
    } else {
        MPI_File_write(matrix_file, matrix.matrix_pointer, matrix.rows_count * matrix.column_count, MPI_DOUBLE, MPI_STATUS_IGNORE);
    }
    
    MPI_File_close(&matrix_file);
    file_io_spent += MPI_Wtime() - start;
}

Matrix<double> read_sub_matrix_stairs(MPI_Comm comm, int along_coord, int i, int j, int z, int dim, int rank,
                                      const char *filename, int *whole_matrix_rows_count, int *whole_matrix_column_count) {
    MPI_Comm stairs;
    MPI_Comm_split(comm, along_coord == z, rank, &stairs);
    Matrix<double> ret;
    if (along_coord == z) {
        sub_matrix_file_op(stairs, i, j, dim, filename, READ, ret, whole_matrix_rows_count, whole_matrix_column_count);
    }
    MPI_Comm_free(&stairs);
    return ret;
}

void broadcast_matrix(Matrix<double> &matrix, int root, MPI_Comm comm) {
    int n_and_m[] = {matrix.rows_count, matrix.column_count};
    MPI_Bcast(n_and_m, 2, MPI_INT, root, comm);
    if (matrix.matrix_pointer == NULL) {
        matrix = Matrix<double>(n_and_m[0], n_and_m[1]);
    }
    MPI_Bcast(matrix.matrix_pointer, matrix.rows_count*matrix.column_count, MPI_DOUBLE, root, comm);
}

void firstStageMatrixMultiplication(int argc, char **argv) {
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int dim = (int) cbrt(size);
    if (rank == 0 && dim*dim*dim != size) {
        fprintf(stderr, "Not correct size: %d\n", size);
        exit(1);
    }
    int dims[] = {dim, dim, dim};
    int periods[] = {0, 0, 0};
    MPI_Comm new_world;
    MPI_Cart_create(MPI_COMM_WORLD, NDIMS, dims, periods, 1, &new_world);

    MPI_Comm_rank(new_world, &rank);
    int coords[NDIMS];
    MPI_Cart_coords(new_world, rank, NDIMS, coords);
    int z = coords[0];
    int i = coords[1];
    int j = coords[2];

    int output_rows_count, output_column_count;

    Matrix<double> A = read_sub_matrix_stairs(new_world, j, i, j, z, dim, rank, argv[1], &output_rows_count, NULL);
    Matrix<double> B = read_sub_matrix_stairs(new_world, i, i, j, z, dim, rank, argv[2], NULL, &output_column_count);

    MPI_Barrier(new_world);

    double start = MPI_Wtime();

    MPI_Comm along_row_comm;
    MPI_Comm_split(new_world, z*dim + i, rank, &along_row_comm);

    MPI_Comm column_comm;
    MPI_Comm_split(new_world, z*dim + j, rank, &column_comm);

    broadcast_matrix(A, z, along_row_comm);
    broadcast_matrix(B, z, column_comm);

    MPI_Comm_free(&along_row_comm);
    MPI_Comm_free(&column_comm);

    Matrix<double> C = A * B;

    Matrix<double> recv;
    if (z == 0) {
        recv = Matrix<double>(C.rows_count, C.column_count);
    }
    MPI_Comm along_z_axis_comm;
    MPI_Comm_split(new_world, i*dim + j, rank, &along_z_axis_comm);

    MPI_Reduce(C.matrix_pointer, recv.matrix_pointer, C.rows_count * C.column_count, MPI_DOUBLE,
            MPI_SUM, 0, along_z_axis_comm);

    MPI_Comm_free(&along_z_axis_comm);

    MPI_Comm z_comm;
    MPI_Comm_split(new_world, z, rank, &z_comm);
    double end;
    if (z == 0) {
        MPI_Bcast(&output_rows_count, 1, MPI_INT, 0, z_comm);
        MPI_Bcast(&output_column_count, 1, MPI_INT, 0, z_comm);
        end = MPI_Wtime();
        MPI_Barrier(new_world);
        sub_matrix_file_op(z_comm, i, j, dim, argv[3], WRITE, recv, &output_rows_count, &output_column_count);
        recv.free();
    } else {
        end = MPI_Wtime();
        MPI_Barrier(new_world);
    }
    MPI_Comm_free(&z_comm);

    A.free();
    B.free();
    C.free();

    if (rank == 0) {
        double max = end - start;
        for (int i = 1; i < size; ++i) {
            double curr;
            MPI_Recv(&curr, 1, MPI_DOUBLE, i, 0, new_world, MPI_STATUS_IGNORE);
            max = std::max(curr, max);
        }
        double file_io_max = file_io_spent;
        for (int i = 1; i < size; ++i) {
            double curr;
            MPI_Recv(&curr, 1, MPI_DOUBLE, i, 0, new_world, MPI_STATUS_IGNORE);
            file_io_max = std::max(curr, file_io_max);
        }
        printf("Spent in seconds: %lf\nFile IO in seconds: %lf\n", max, file_io_max);
    } else {
        double time = end - start;
        MPI_Send(&time, 1, MPI_DOUBLE, 0, 0, new_world);
        MPI_Send(&file_io_spent, 1, MPI_DOUBLE, 0, 0, new_world);
    }
}

/////////////////////////////////////////
// SECOND STAGE - PRIME NUMBERS SEARCH //
/////////////////////////////////////////

typedef int Number;
const MPI_Datatype number_datatype = MPI_INT;

using namespace std;

class SiefOfEratosthenes {
    vector<bool> is_prime;
    Number start;
    Number cur_index;
public:
    set<Number> prime_numbers;

    SiefOfEratosthenes(Number start, Number end) : start(start), cur_index(0) {
        is_prime = vector<bool>(end - start, true);
    }

    Number calc_next_prime()  {
        for (; cur_index < (Number)is_prime.size(); ++cur_index) {
            if (is_prime[cur_index]) {
                Number prime = cur_index + start;
                prime_numbers.insert(prime);
                for (int i = cur_index; i < (Number)is_prime.size(); i += prime) {
                    is_prime[i] = false;
                }
                return prime;
            }
        }
        return -1;
    }
};

void print_to_file_process(const char *file_path, int number_of_workers) {
    int exits_count = 0;
    vector<Number> primes;
    unsigned long long sum_process_time = 0;
    Number process_max_time = -1;
    while (true) {
        Number prime;
        MPI_Status status;
        MPI_Recv(&prime, 1, number_datatype, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (prime <= 0) {
            process_max_time = std::max(process_max_time, std::abs(prime));
            sum_process_time += std::abs(prime);
            if (++exits_count == number_of_workers) {
                break;
            }
        } else {
            primes.push_back(prime);
        }
    }
    cout << "Sum spent of all processes in seconds: " << sum_process_time << endl;
    cout << "Spent time of longest process in seconds: " << process_max_time << endl;

    ofstream output(file_path);
    output << "Number of prime numbers: " << primes.size() << endl;
    output << "Prime numbers: " << endl;
    for (std::vector<Number>::iterator i = primes.begin(); i < primes.end(); ++i) {
        output << *i << '\n';
    }
}

void process(Number a, Number b, int worker_id, int number_of_workers) {
    clock_t start_time = clock();
    Number k = (b - a) / number_of_workers;
    Number start = a + worker_id*k;
    Number end = (worker_id == number_of_workers - 1 ? b : start + k);

    Number end_sqrt = (Number)sqrt(end);
    SiefOfEratosthenes sief(2, (end_sqrt + 1));
    {
        Number prime;
        do {
            prime = sief.calc_next_prime();
            if (prime >= start && prime < end) {
                MPI_Send(&prime, 1, number_datatype, 0, 0, MPI_COMM_WORLD);
            }
        } while(prime > 0);
    }

    start = max(start, end_sqrt + 1);

    for (Number number = start; number < end; ++number) {
        bool is_continue = false;
        for (set<Number>::iterator i = sief.prime_numbers.begin(); i != sief.prime_numbers.end(); ++i) {
            Number prime = *i;
            if (number % prime == 0) {
                is_continue = true;
                break;
            }
        }
        if (is_continue) {
            continue;
        }
        MPI_Send(&number, 1, number_datatype, 0, 0, MPI_COMM_WORLD);
    }
    clock_t spent = (clock() - start_time)/CLOCKS_PER_SEC;
    Number exit = (Number) -1 * spent;
    MPI_Send(&exit, 1, number_datatype, 0, 0, MPI_COMM_WORLD);
}

void secondStagePrimeNumbersSearch(int argc, char **argv) {
    int size, id;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    Number a = atoi(argv[1]);
    Number b = atoi(argv[2]) + 1;
    if (a < 2) {
        a = 2;
    }
    if (b < 2) {
        b = 2;
    }
    if (b <= a) {
        if (id == 0) {
            cout << "Not valid range" << endl;
        }
        exit(1);
    }

    int worker_id = id - 1;
    int number_of_workers = size - 1;
    if (id == 0) {
        print_to_file_process(argv[3], number_of_workers);
    } else {
        process(a, b, worker_id, number_of_workers);
    }
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (argc - 1 != 6 && rank == 0) {
        fprintf(stderr, "Usage: %s\n A B C a b output-file\n"
                        "First 3 args are for first stage - parallel matrix multiplication\n"
                        "Second 3 args are for second stage - prime numbers search\n", argv[0]);
        exit(1);
    }
    firstStageMatrixMultiplication(argc, argv);
    secondStagePrimeNumbersSearch(argc, argv + 3);
    MPI_Finalize();
}
