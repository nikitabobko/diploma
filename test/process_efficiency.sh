#!/usr/bin/env sh

sizes=("512x512" "1024x1024" "2048x2048" "1024x4096" "4096x1024" "4096x4096")
processes=(2 32 64 128 256 512)

for size in ${sizes[@]}; do 
    m=$(echo $size | cut -d'x' -f1)
    n=$(echo $size | cut -d'x' -f2)

    start=$(cat ${size}_2_fixed.output)

    x=$(cat ${size}__mapping.output)
    x=$(echo "print(${start}/${x})" | python)
    var="${m} & ${n} & $x"
    for p in ${processes[@]}; do
        x=$(cat ${size}_${p}_fixed.output)
        x=$(echo "print(${start}/${x}/${p})" | python)
        var+=" & ${x}"
    done
    echo "$var \\\\"
    echo "\hline"
done