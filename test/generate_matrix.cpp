#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include "util.cpp"

int main(int argc, char const *argv[]) {
    if (argc - 1 != 4) {
        printf("Usage: %s <random> <n> <m> <output-binary-filename>\n", argv[0]);
        exit(1);
    }

    bool random = bool(atoi(argv[1]));
    int n = atoi(argv[2]);
    int m = atoi(argv[3]);
    std::ofstream output_file(argv[4], std::ios::binary);

    write_to_binary(output_file, 'd');
    write_to_binary(output_file, n);
    write_to_binary(output_file, m);

    srand(time(0));

    loop(i, n) {
        loop(j, m) {
            double num;
            if (random) {
                num = rand() + rand() / (double)RAND_MAX;
            } else {
                std::cin >> num;
            }
            write_to_binary(output_file, num);
        }
    }
    return 0;
}
