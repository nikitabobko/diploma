#!/usr/bin/env sh

sizes=("1024x1024" "2048x2048" "4096x4096")
processes=(1 8 64 125)

for size in ${sizes[@]}; do 
    m=$(echo $size | cut -d'x' -f1)
    n=$(echo $size | cut -d'x' -f1)
    var="${m} & ${n} & $(cat results/${size}_125_mapping.stdout | head -n 1)"
    for p in ${processes[@]}; do
        var+=" & $(cat results/${size}_${p}.stdout | head -n 1)"
    done
    echo "$var \\\\"
    echo "\hline"
done
