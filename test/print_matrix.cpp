#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include "util.cpp"

template<typename T>
void print(const Matrix<T> &matrix, std::ostream &output) {
    loop(i, matrix.rows_count) {
        loop(j, matrix.column_count) {
            output << matrix.at(i, j) << " ";
        }
        output << std::endl;
    }
}

int main(int argc, char const *argv[]) {
    if (argc - 1 != 1) {
        printf("Usage: %s <input-binary-filename>\n", argv[0]);
        exit(1);
    }
    std::ifstream input_file(argv[1], std::ios::binary);

    uchar type = read_from_binary<uchar>(input_file);
    std::cout << "Type is: " << type << std::endl;
    if (type == 'f') {
        print(read_float_matrix_from_binary_stream(input_file), std::cout);
    } else {
        print(read_double_matrix_from_binary_stream(input_file), std::cout);
    }
}
