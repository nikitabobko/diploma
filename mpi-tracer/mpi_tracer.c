#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <mpi.h>
#include <execinfo.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>

static int globalRank = -1;
static int globalSize = -1;

static int *sendHits = NULL;

static char globalTracedDataPath[PATH_MAX + 1];

static int globalBatchId = 0;

static const int DEFAULT_MIN_BATCH_SIZE = 100;

////////////////////
// REASON REGULAR //
////////////////////
static const char *REASON_REGULAR = "REGULAR";
static int globalCurrentRegularReasonBatchSize = 0;
static const int MIN_BATCH_SIZE_FOR_REGULAR_REASON = DEFAULT_MIN_BATCH_SIZE;

/////////////////////////
// REASON MPI_FINALIZE //
/////////////////////////
static const char *REASON_MPI_FINALIZE = "MPI_FINALIZE";

static void fail(const char *format, ...) {
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(1);
}

static void printBacktraceJsonToFile(FILE *file) {
  if (file == NULL) {
    fail("`file` arg isn't supposed to be NULL\n");
  }
  int TRACE_CAPACITY = 16;
  void *trace[TRACE_CAPACITY];
  char **messages = NULL;

  int traceSize = backtrace(trace, TRACE_CAPACITY);
  messages = backtrace_symbols(trace, traceSize);
  fprintf(file, "[");
  for (int i = 0; i < traceSize; ++i) {
    if (i == traceSize - 1) {
      fprintf(file, "\"%s\"", messages[i]);
    } else {
      fprintf(file, "\"%s\",", messages[i]);
    }
  }
  fprintf(file, "]");
}

static int getMpiWorldRankLazy() {
  if (globalRank < 0) {
    MPI_Comm_rank(MPI_COMM_WORLD, &globalRank);
    if (globalRank < 0) {
      fail("Negative rank?! Either something went wrong\n");
    }
  }
  return globalRank;
}

static int getMpiWorldSizeLazy() {
  if (globalSize < 0) {
    MPI_Comm_size(MPI_COMM_WORLD, &globalSize);
    if (globalSize < 0) {
      fail("Something went wrong determining COMM_WORLD size\n");
    }
  }
  return globalSize;
}

static int translateToWorldRank(MPI_Comm comm, int localRank) {
  if (comm == MPI_COMM_WORLD) {
    // optimization (impact isn't measured)
    return localRank;
  }
  MPI_Group worldGroup;
  MPI_Group localGroup;
  MPI_Comm_group(MPI_COMM_WORLD, &worldGroup);
  MPI_Comm_group(comm, &localGroup);

  int globalRank;
  MPI_Group_translate_ranks(localGroup, 1, &localRank, worldGroup, &globalRank);
  return globalRank;
}

static long long currentTimeMillis() {
  struct timeval te;
  gettimeofday(&te, NULL); // get current time
  long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
  return milliseconds;
}

static void dumpCurrentTrace(const char* reason) {
  char path[8192];

  sprintf(path, "%s/dump_rank-%04d_batch-%04d.json", globalTracedDataPath, getMpiWorldRankLazy(), globalBatchId);
  FILE *hitsFile = fopen(path, "w");
  {
    if (hitsFile == NULL) {
      fail("Cannot create file %s\n", path);
    }
    fprintf(hitsFile, "{\"hits\":[");
    int mpiSize = getMpiWorldSizeLazy();
    for (int i = 0; i < mpiSize; ++i) {
      if (i == mpiSize - 1) {
        fprintf(hitsFile, "%d", sendHits[i]);
      } else {
        fprintf(hitsFile, "%d,", sendHits[i]);
      }
      sendHits[i] = 0;
    }
    fprintf(hitsFile, "]");
    fprintf(hitsFile, ",\"reason\":\"%s\"", reason);
    fprintf(hitsFile, ",\"timeMillis\":%lld", currentTimeMillis());
    fprintf(hitsFile, ",\"trace\":");
    printBacktraceJsonToFile(hitsFile);
    fprintf(hitsFile, "}\n");
    fclose(hitsFile);
  }
  globalBatchId++;

  // reset counters
  if (reason == REASON_REGULAR) {
    // nothing to do
  } else if (reason == REASON_MPI_FINALIZE) {
    // nothing to do
  } else {
    fail("Unknown dump reason");
  }
  // reset counter for REGULAR_REASON on every dump
  globalCurrentRegularReasonBatchSize = 0;
}

static void processSend(int sendSize) {
  if (globalCurrentRegularReasonBatchSize >= MIN_BATCH_SIZE_FOR_REGULAR_REASON) {
    dumpCurrentTrace(REASON_REGULAR);
  }
  globalCurrentRegularReasonBatchSize += sendSize;
}

static int getRank(MPI_Comm comm) {
  int rank = 0;
  MPI_Comm_rank(comm, &rank);
  return rank;
}

static int getSize(MPI_Comm comm) {
  int size = 0;
  MPI_Comm_size(comm, &size);
  return size;
}

static int getTypeSize(MPI_Datatype type) {
  int size = 0;
  MPI_Type_size(type, &size);
  return size;
}

static void allToAllPattern(MPI_Comm comm, int count, MPI_Datatype datatype) {
  int size = getSize(comm);
  int typeSize = getTypeSize(datatype);
  for (int i = 0; i < size; ++i) {
    sendHits[translateToWorldRank(comm, i)] += count * typeSize;
  }
  processSend(size*count*typeSize);
}

static void sendPattern(MPI_Comm comm, int count, MPI_Datatype datatype, int dest) {
  int typeSize = getTypeSize(datatype);
  sendHits[translateToWorldRank(comm, dest)] += count * typeSize;
  processSend(count * typeSize);
}

static void broadcastPattern(MPI_Comm comm, int count, MPI_Datatype datatype, int root) {
  int rank = getRank(comm);
  if (rank == root) {
    int size = getSize(comm);
    int typeSize = getTypeSize(datatype);
    for (int i = 0; i < size; ++i) {
      sendHits[translateToWorldRank(comm, i)] += count * typeSize;
    }
    processSend(size * count * typeSize);
  }
}

///////////////////
// Tracer itself //
///////////////////

int MPI_Init(int *argc, char ***argv) {
  int ret = PMPI_Init(argc, argv);

  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char fileName[4096];
  sprintf(fileName, "./mpi_tracer_dumps_%04d-%02d-%02d_%02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1,
          tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

  if (getMpiWorldRankLazy() == 0) {
    struct stat st = {0};
    if (stat(fileName, &st) == -1) {
      mkdir(fileName, 0700);
    } else {
      fail("Directory for traced data: %s already exist\n", fileName);
    }
  }
  PMPI_Bcast(fileName, sizeof(fileName)/sizeof(*fileName), MPI_CHAR, 0, MPI_COMM_WORLD);
  if (realpath(fileName, globalTracedDataPath) == NULL) {
    perror("realpath failed\n"); exit(1);
  }
  sendHits = calloc(getMpiWorldSizeLazy(), sizeof(*sendHits));

  return ret;
}

int MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm) {
  sendPattern(comm, count, datatype, dest);
  return PMPI_Send(buf, count, datatype, dest, tag, comm);
}

int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm) {
  broadcastPattern(comm, count, datatype, root);
  return PMPI_Bcast(buffer, count, datatype, root, comm);
}

int MPI_Allgather(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount,
  MPI_Datatype recvtype, MPI_Comm comm
) {
  allToAllPattern(comm, sendcount, sendtype);
  return PMPI_Allgather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
}

int MPI_Allgatherv(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, const int recvcounts[],
  const int displs[], MPI_Datatype recvtype, MPI_Comm comm
) {
  allToAllPattern(comm, sendcount, sendtype);
  return PMPI_Allgatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, comm);
}

int MPI_Allreduce(
  const void *sendbuf, void *recvbuf, int count,
  MPI_Datatype datatype, MPI_Op op, MPI_Comm comm
) {
  allToAllPattern(comm, count, datatype);
  return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
}

int MPI_Alltoall(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount,
  MPI_Datatype recvtype, MPI_Comm comm
) {
  allToAllPattern(comm, sendcount, sendtype);
  return PMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
}

int MPI_Alltoallw(
  const void *sendbuf, const int sendcounts[], const int sdispls[], const MPI_Datatype sendtypes[],
  void *recvbuf, const int recvcounts[], const int rdispls[], const MPI_Datatype recvtypes[],
  MPI_Comm comm
) {
  int size = getSize(comm);
  int amountSendSize = 0;
  for (int i = 0; i < size; ++i) {
    int tmp = sendcounts[i] * getTypeSize(sendtypes[i]);
    amountSendSize += tmp;
    sendHits[translateToWorldRank(comm, i)] += tmp;
  }
  processSend(amountSendSize);
  return PMPI_Alltoallw(sendbuf, sendcounts, sdispls, sendtypes, recvbuf, recvcounts, rdispls, recvtypes, comm);
}

int MPI_Bsend(
  const void *buf, int count, MPI_Datatype datatype,
  int dest, int tag, MPI_Comm comm
) {
  sendPattern(comm, count, datatype, dest);
  return PMPI_Bsend(buf, count, datatype, dest, tag, comm);
}

int MPI_Gather(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount, MPI_Datatype recvtype,
  int root, MPI_Comm comm
) {
  sendPattern(comm, sendcount, sendtype, root);
  return PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
}

int MPI_Gatherv(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, const int recvcounts[], const int displs[],
  MPI_Datatype recvtype, int root, MPI_Comm comm
) {
  sendPattern(comm, sendcount, sendtype, root);
  return PMPI_Gatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, root, comm);
}

int MPI_Reduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm) {
  sendPattern(comm, count, datatype, root);
  return PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
}

int MPI_Scatter(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount, MPI_Datatype recvtype,
  int root, MPI_Comm comm
) {
  broadcastPattern(comm, sendcount, sendtype, root);
  return PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
}

int MPI_Scatterv(
  const void *sendbuf, const int sendcounts[], const int displs[],
  MPI_Datatype sendtype, void *recvbuf, int recvcount,
  MPI_Datatype recvtype, int root, MPI_Comm comm
) {
  int rank = getRank(comm);
  if (rank == root) {
    int size = getSize(comm);
    int typeSize = getTypeSize(sendtype);
    int amount = 0;
    for (int i = 0; i < size; ++i) {
      int tmp = sendcounts[i] * typeSize;
      amount += tmp;
      sendHits[translateToWorldRank(comm, i)] += tmp;
    }
    processSend(amount);
  }
  return PMPI_Scatterv(sendbuf, sendcounts, displs, sendtype, recvbuf, recvcount, recvtype, root, comm);
}

int MPI_Sendrecv(
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,
  int dest, int sendtag, void *recvbuf, int recvcount,
  MPI_Datatype recvtype, int source, int recvtag,
  MPI_Comm comm,  MPI_Status *status
) {
  sendPattern(comm, sendcount, sendtype, dest);
  return PMPI_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag, recvbuf, recvcount, recvtype, source, recvtag, comm, status);
}

int MPI_Finalize() {
  dumpCurrentTrace(REASON_MPI_FINALIZE);
  if (getMpiWorldRankLazy() == 0) {
    printf("!!! MPI Tracer: traced data saved to: %s\n", globalTracedDataPath);
  }

  int ret = PMPI_Finalize();

  free(sendHits);
  sendHits = NULL;

  return ret;
}
