buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.71")
  }
}

plugins {
  id("org.jetbrains.kotlin.jvm") version "1.3.71"

  application
}

repositories {
  jcenter()
  mavenCentral()
}

dependencies {
  implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("com.xenomachina:kotlin-argparser:2.0.7")
  implementation("com.google.code.gson:gson:2.8.6")

  testImplementation("org.jetbrains.kotlin:kotlin-test")

  testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

tasks.getByName("jar", Jar::class) {
  manifest {
    attributes(
      "Main-Class" to "diploma.MainKt",
      "Class-Path" to configurations.compile.filter { true }.map { it.name })
  }
  from(configurations.compileClasspath.filter { true }.map { if (it.isDirectory) it else zipTree(it) })
}

application {
  mainClassName = "diploma.MainKt"
}
